#!/bin/bash
# Copyright (C) 2020 Christopher Engelhard
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#
# generate_pem.sh
# generate .pem-encoded FFDHE parameters from standardized constants
#

VERSION='202008.1'
DEFDIR='./definitions'
DEFEXT='.def'

unset exclude
unset includelist
unset excludelist

usage() {
  cat <<-EOF
	Usage: generate_pem.sh [options]
	
	Options:
	  -h   display this help
	  -d   specify directory to read definitions from, default ./definitions
	  -o   specify output directory, default .
	  -e   comma-separated list of group names to exclude
	  -i   comma-separated list of group names to include. Only these
	       are processed, -e is ignored
	  -m   -i,-e only applies to the moduli file
	
	EOF
}

genfiles() {
  echo "Generating files..."

  # create some temporary files
  asnfile=$(mktemp)
  derfile=$(mktemp)
  pemfile=$(mktemp)
  modulifile=$(mktemp)

  for deffile in ${DEFDIR}/*${DEFEXT}; do
    [[ -f $deffile ]] || break
    echo "Reading $deffile"
    source $deffile
    if [[ $includelist && ! $includelist = *$NAME* || $excludelist = *$NAME* ]]; then
      exclude=0
    fi
    if [[ $exclude ]] && [[ ! $modulionly ]]; then
      echo "Skipping DH group $NAME from $DEFINED_IN, excluded"
      unset exclude
    else
      echo "Processing DH group $NAME from $DEFINED_IN, retrieved $TIMESTAMP"
      # strip whitespace from P and G
      P=$( tr -d '[:space:]' <<< "$P" )
      G=$( tr -d '[:space:]' <<< "$G" )
      # format and append prime to OpenSSH moduli file
      if [[ ! $exclude ]]; then
	echo "# $NAME" >> "$modulifile"
	echo "Adding group to moduli file."
	echo "${TIMESTAMP}000000 $TYPE 6 100 $(( bitsize-1 )) $G $P" >> "$modulifile"
      else
	echo "Not adding group to moduli file, excluded."
      fi
      # create ASN.1 template file for OpenSSL .pem parameters
      cat <<-EOF > "$asnfile"
	asn1=SEQUENCE:seq_sect
	
	[seq_sect]
	
	field1=INTEGER:0x${P}
	field2=INTEGER:0x0${G}
	EOF
      # convert ASN.1 template to DER binary format
      openssl asn1parse -genconf "$asnfile" -noout -out "$derfile"
      # convert DER binary to PEM ASCII format
      openssl dhparam -inform der -in "$derfile" -outform pem -out "$pemfile"
      # copy temporary pemfile to output file
      echo "Saving to ${outdir}$NAME.pem"
      cp "$pemfile" "${outdir}$NAME.pem"
    fi
    echo
  done

  echo "Creating moduli file ${outdir}moduli.template."
  cp "$modulifile" "${outdir}moduli.template"

  echo "Cleaning up."
  rm $asnfile
  rm $derfile
  rm $pemfile
  rm $modulifile
}

echo "generate_pem.sh  - version $VERSION"
while getopts 'hd:o:e:i:m' flag; do
  case $flag in
    h)
      usage
      exit 0
      ;;
    d)
      DEFDIR="${OPTARG%/}"
      ;;
    o)
      outdir="${OPTARG%/}/"
      ;;
    e)
      excludelist=$OPTARG
      ;;
    i)
      includelist=$OPTARG
      ;;
    m)
      modulionly=0
      ;;
    ?)
      echo "Usage: generate_pem.sh [-h] [-m] [-d <dir>] [-o <dir>] [-e <list>] [-i <list>]"
      exit 1
      ;;
  esac
done
shift $(( OPTIND - 1 ))

echo
genfiles
echo "Done."
