# Definition files for standardized FFDHE parameters

This directory contains the definitions of all shipped FFDHE prime groups in human-readable form,
as well the texts of the RFCs they are defined in.

All RFCs were retrieved from https://tools.ietf.org/rfc/ on the date specified in the .def files.
