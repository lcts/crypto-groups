# crypto-groups - standardized Finite-Field Diffie-Hellman Ephemeral parameters

**Current version: 202008.1**

This repository contains definitions for the various standardized Finite-Field
Diffie-Hellman Ephemeral groups as defined in RFCs, including those required
by TLSv1.3 and FIPS standards, to simplify the use of these groups in standard
server software etc. .

Currently contains:

 - DH groups 1&2 (modp768 & modp1024) from RFC7296
 - DH groups 5,14-18 (modp1536-modp8192) from RFC3526
 - DH groups 22-24 from RFC5114
 - ffdhe2048-ffdhe8196 from RFC7919

## Installation

Clone the repository or extract the tarball somewhere on your machine and chdir into the
`crypto-groups` directory. Generate PEM-encoded `.pem` files and a moduli file from the
definitions by executing `./generate_pem.sh`. The moduli file is created as `moduli.template`
and contains all groups. Please remove all groups you do not want to use from it before
copying it to it's final destination.

## Parameter Verification

All parameters are defined in human-readable form in files within the `definitions` directory.
The parameters in these files can be directly compared to those specified in the official RFC
documents. I strongly advise that you do this.

## Should I use all of these?

Some of the groups included are shipped mainly for historical & compatability reasons and
should probably be avoided where possible. This includes

 - all groups with 1024 bit primes or less (modp768-group1, modp1024-group2, modp1024-group22),
   because 1024 bit prime groups are small enough to allow pre-computation attacks with current
   hardware
 - other groups from RFC5114 (modp2048-group23 and modp2048-group24), as they are not widely used,
   not safe (in the mathematical sense) and derived from unexplained constants.
